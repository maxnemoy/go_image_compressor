package core

import (
	"fmt"
	"github.com/anthonynsimon/bild/blur"
	"github.com/anthonynsimon/bild/imgio"
	"github.com/anthonynsimon/bild/transform"
	"image"
	"os"
	"sync"
	"time"
)



//func main()  {
//	baner := "Сжиматель 3000"
//	fmt.Println(baner)
//
//
//	if _, err := os.Stat("./Compressed"); os.IsNotExist(err) {
//		fmt.Println("Создаю папку")
//		os.MkdirAll("./Compressed", os.ModePerm)
//	}
//	files, err := ioutil.ReadDir("./")
//
//	if err != nil {
//		fmt.Println("line 25: ", err)
//		time.Sleep(time.Second * 20)
//	}
//
//	all_size := int64(0)
//
//	for i, f := range files {
//		ext := strings.Split(f.Name(), ".")
//		if len(ext) > 0{
//			if  ext[len(ext)-1] == "jpg" || ext[len(ext)-1] == "jpeg" || ext[len(ext)-1] == "JPG"  {
//				all_size = all_size + f.Size()
//				fmt.Println(i+1, "/", len(files)," Сжимаю: ", f.Name())
//				file, err := os.Open(f.Name()) // For read access.
//				if err != nil {
//					fmt.Println("line 38: ", err)
//					time.Sleep(time.Second * 20)
//				}
//
//				fName := strings.Split(f.Name(), ".")
//				//go ImageProcessing(file, "./Compressed/", fName[0], fName[len(ext)-1], )
//
//			}
//		} else {
//			fmt.Println("Нет файлов для обработки")
//		}
//
//	}
//
//
//
//	cmp_size := int64(0)
//	cmp, err := ioutil.ReadDir("./Compressed/")
//	if err != nil {
//		fmt.Println("line 53: ", err)
//		time.Sleep(time.Second * 20)
//	}
//
//	for _, f := range cmp {
//		ext := strings.Split(f.Name(), ".")
//		if "jpg" == ext[1] {
//			cmp_size = cmp_size + f.Size()
//		}
//	}
//
//	fmt.Println("Общий размер до обработки примерно ", all_size / 1048576, " мегабайт")
//	fmt.Println("Общий размер после обработки примерно ", cmp_size / 1048576, " мегабайт")
//	fmt.Println("Экономия примерно ", (all_size / 1048576) - (cmp_size / 1048576), " мегабайт")
//
//	time.Sleep(time.Second * 20)
//
//}


func ImageProcessing(file *os.File, path string, fName string, ext string, wg *sync.WaitGroup) {
	defer wg.Done()
	defer fmt.Println("DONE")
	fmt.Println("RUN")
	var width int
	s, _, err := image.DecodeConfig(file)
	if err != nil {
		fmt.Println("line 76: ", err)
		time.Sleep(time.Second * 20)
	}

	if s.Width >= 1000 {
		width = 1000
	} else {
		width = s.Width
	}

	img, err := imgio.Open(file.Name())
	if err != nil {
		fmt.Println("line 84: ", err)
		time.Sleep(time.Second * 20)
	}

	height := calculateHeidth(s.Width, s.Height, width)
	filePathAndName := path + "h_" + fName
	saveTmpImage(img, width, height, filePathAndName, false)
	os.RemoveAll("./" + fName + "." + ext)

}

func saveTmpImage(img image.Image, width int, height int, filePathAndName string, blurEffect bool) {
	imgProcessing := transform.Resize(img, width, height, transform.Lanczos)
	if blurEffect {
		imgProcessing = blur.Gaussian(imgProcessing, 5.0)
	}

	// Or imgio.JPEGEncoder(95) as encoder for JPG with quality of 95%
	if err := imgio.Save(filePathAndName + ".jpg", imgProcessing, imgio.JPEGEncoder(50)); err != nil {
		if err != nil {
			fmt.Println("line 108: ", err)
			time.Sleep(time.Second * 20)
		}
	}
}

func calculateHeidth(oldWidth, oldHeight, newWidth int) int {
	return oldHeight * newWidth / oldWidth
}

