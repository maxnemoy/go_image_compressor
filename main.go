package main

import (
	"fmt"
	"github.com/gen2brain/go-fitz"
	"github.com/anthonynsimon/bild/blur"
	"github.com/anthonynsimon/bild/imgio"
	"github.com/anthonynsimon/bild/transform"
	"github.com/schollz/progressbar"
	"image"
	"image/jpeg"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

var workerCount int
var wg sync.WaitGroup

func progress(max int) *progressbar.ProgressBar {
	bar := progressbar.New(max)
	bar.RenderBlank() // will show the progress bar
	return bar
}

var baner = "IMAGE COMPRESSOR on golang, by maxnemoy"

func main() {
	fmt.Println(baner)
	//workerCount := 0
	//photoCount := 0
	//pb := progress(photoCount)
	abs,err := filepath.Abs("./")
	if err != nil {
		fmt.Println("Path error")
	}

	if _, err := os.Stat(abs+"/Compressed"); os.IsNotExist(err) {
		fmt.Println("Create Folder...")
		os.MkdirAll(abs+"/Compressed", os.ModePerm)
	}

	files, err := ioutil.ReadDir("./")
	for _, f := range files {
		ext := strings.Split(f.Name(), ".")
		if !f.IsDir() {
			switch strings.ToLower(ext[len(ext)-1]) {
			case "jpg":
				img(f.Name(), ext[len(ext)-1])
				break
			case "jpeg":
				img(f.Name(), ext[len(ext)-1])
				break
			case "png":
				img(f.Name(), ext[len(ext)-1])
				break
			case "pdf":
				pdfToJPG(f.Name())
				break

			}
		}
	}

	if err != nil {
		fmt.Println("line 25: ", err)
		time.Sleep(time.Second * 20)
	}
	wg.Wait()
	fmt.Println("\n>>>>>>>>DONE")
	for j := 10; j >= 0; j-- {
		time.Sleep(time.Second)
	}

}

func img(name string, ext string) {
	file, err := os.Open(name) // For read access.
	if err != nil {
		fmt.Println("line 38: ", err)
		time.Sleep(time.Second * 20)
	}

	workerCount++
	wg.Add(1)
	abs,err := filepath.Abs("./")
	if err != nil {
		fmt.Println("Path error")
	}
	go imageProcessing(file, abs+"/Compressed/", name, ext, &wg)

	if workerCount > 3 {
		wg.Wait()
	}

}

func doneW() {
	workerCount--
}
func imageProcessing(file *os.File, path string, fName string, ext string, wg *sync.WaitGroup) {
	defer wg.Done()
	defer fmt.Println("DONE")
	defer doneW()
	fmt.Println("RUN")
	var width int
	s, _, err := image.DecodeConfig(file)
	if err != nil {
		fmt.Println("line 76: ", err)
		time.Sleep(time.Second * 20)
	}

	if s.Width >= 1000 {
		width = 1000
	} else {
		width = s.Width
	}

	img, err := imgio.Open(file.Name())
	if err != nil {
		fmt.Println("line 84: ", err)
		time.Sleep(time.Second * 20)
	}

	height := calculateHeidth(s.Width, s.Height, width)
	filePathAndName := path + "h_" + fName
	saveTmpImage(img, width, height, filePathAndName, false)
	//os.RemoveAll("./" + fName + "." + ext)

}

func saveTmpImage(img image.Image, width int, height int, filePathAndName string, blurEffect bool) {
	imgProcessing := transform.Resize(img, width, height, transform.Lanczos)
	if blurEffect {
		imgProcessing = blur.Gaussian(imgProcessing, 5.0)
	}

	// Or imgio.JPEGEncoder(95) as encoder for JPG with quality of 95%
	if err := imgio.Save(filePathAndName+".jpg", imgProcessing, imgio.JPEGEncoder(50)); err != nil {
		if err != nil {
			fmt.Println("line 108: ", err)
			time.Sleep(time.Second * 20)
		}
	}
}

func calculateHeidth(oldWidth, oldHeight, newWidth int) int {
	return oldHeight * newWidth / oldWidth
}

func pdfToJPG(name string)  {
	doc, err := fitz.New(name)
	defer doc.Close()
	abs,err := filepath.Abs("./")
	if err != nil {
		fmt.Println("Path error")
	}

	if err != nil {
		panic(err)
	}
	nameF := strings.Split(name, ".")
	path := abs+"/"+nameF[0] + "/"

	os.MkdirAll(path, os.ModePerm)

	for n := 0; n < doc.NumPage(); n++ {
		imagePdf, err := doc.Image(n)
		if err != nil {
			panic(err)
		}


		f, err := os.Create(filepath.Join(path, fmt.Sprintf("%v_%v.jpg",nameF[0], n)))
		if err != nil {
			panic(err)
		}

		err = jpeg.Encode(f, imagePdf, &jpeg.Options{Quality: 50})
		if err != nil {
			panic(err)
		}

		nameF2 := strings.Split(name, ".")
		img(f.Name(), nameF2[len(nameF2)-1])

		f.Close()
	}
}
